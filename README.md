BB coding exercise
==================

General tech used:

*   Lerna, Yarn workspaces, Babel
*   ES2017
*   Zeit/Now/Vercel for instant deployment
*   Gitlab CI for tests
*   lerna & yarn are already setup to push any modules to NPM if need be

[Question 1](/css-only-card-flip)
---------------------------------
[Demo](https://bb-code-challenge-ohgskzlgqf.now.sh/css-only-card-flip)

### Technologies used:

*   Zero javascript :)
*   CSS3, CSS Grid and some flex box.
*   CSS Custom properties - heavily.
*   SASS, only for nesting and some very basic mixins (I normally use styled-components for projects)
*   flip effect is complete done using CSS & the checkbox + label sibling selector
*   no hard coded units, they're all CSS Custom properties or a result of a calc()
*   everything follows a vertical grid, multiple of 10px (customizable) What's not done
*   want to implement font-sizing based on calc width so it resizes fluidly without using media queries.
*   A couple of small scripts to get flippable cards scrollable on iOS. It's a known issue I've dealt with before image.png

[Question 2](/flickr-vanilla-javascript):
-----------------------------------------
[Demo](https://bb-code-challenge-ohgskzlgqf.now.sh/flickr-vanilla-javascript)

[code is here](https://gitlab.com/vviikk/bb-coding-xercise/blob/master/packages/flickr-vanilla-javascript/src/js/index.js)

### 

*   Promises / Asysc / Await (you can see I love promises)
*   JsonP (not a fan)
*   Rest
*   other cool tidbits
*   Deploy to now.sh
*   CSS code is a little messy. I focused on precise and structured CSS for question 1, so this one, I wanted to focus on Javascript.

Question 3:
-----------

Code is here: [https://codepen.io/piggyslasher/pen/GYdERQ?editors=0010](https://codepen.io/piggyslasher/pen/GYdERQ?editors=0010)

*   Really wanted to give you a node endpoint hosted on Zeit. In Javascript there's a few libraries that help us do this - the bloated moment.js for instance. Anyway, I don't think my solution is the best way to do it, but I wanted to avoid checking by recursing (i.e. iterate through every day)
*   The code is here
*   Been a few years since PHP but I think it might not be the best time for me to dive back in. I really want to write unit tests. I hope the codepen solution would suffice.

Have a good day.


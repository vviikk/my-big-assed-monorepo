// stolen from babel's jest config
module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    'packages/**/*.{js}',
    '!**/node_modules/**',
  ],
  testMatch: ['./(packages|codemods)/[^/]+/test/.+\\.m?js$', '**/?(*.)+(spec|test).js?(x)'],
  testPathIgnorePatterns: [
    '/node_modules/',
    '/babel-parser/test/expressions/',
    '/test/tmp/',
    '/test/__data__/',
    '/test/helpers/',
    '<rootDir>/test/warning\\.js',
    '<rootDir>/build/',
    '_browser\\.js',
  ],
  testEnvironment: 'node',
  setupTestFrameworkScriptFile: '<rootDir>/test/testSetupFile.js',
  transformIgnorePatterns: [
    '/node_modules/',
    '<rootDir>/(packages|codemods)/[^/]+/lib/',
  ],
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '__fixtures__/',
    'build/',
    '<rootDir>/packages/[^/]+/(__fixtures__|tmp|__data__|__mocks__)?/',
  ],
  modulePathIgnorePatterns: [
    '<rootDir>/build/',
    'npm-cache',
    '.npm',
    '.cache',
  ],
}

module.exports = {
  presets: [
    [
      'env',
      {
        targets: {
          node: 'current',
        },
        jest: true,
        shippedProposals: true,
      },
    ],
  ],
  plugins: [
    '@babel/plugin-proposal-object-rest-spread',
    'transform-util-promisify',
    ['transform-class-properties', { spec: true }],
    [
      'transform-es2015-modules-commonjs',
      {
        spec: true,
      },
    ],
  ],
  sourceMaps: 'inline',
}

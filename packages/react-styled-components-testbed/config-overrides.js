/* config-overrides.js */
const { injectBabelPlugin } = require('react-app-rewired')

module.exports = function override(config, env) {
  // do stuff with the webpack config...
  return injectBabelPlugin('transform-decorators-legacy', config)
}

import { cache, CacheType } from 'cache-decorator'
import DB from './db.json'
import Price from './price/Price'


const price = new Price()

const ttl = 2000
const cacheOptions = { type: CacheType.MEMO, ttl }

/**
 * Gateway for API. Reads from the json database and returns
 * values requested to mock a real API
 *
 * @export
 * @class API
 */
export default class API {
  constructor(db = DB) { // accepts DB as an argument as we want to mock it for tests
    this.getDiscount = this.getDiscount.bind(this)
    this.getItem = this.getItem.bind(this)
    this.getItems = this.getItems.bind(this)
    this.getPrice = this.getPrice.bind(this)
    this.DB = db
  }

  // basically just used if no discount applied
  _defaultDiscount = {
    itemsToGet: 1, itemsToPayFor: 1, discount: 0, minItemsToBuy: 0,
  }

  /**
   * Checks if there is a discount for an item based on the userID
   *
   * @param {*} itemId SKU of the item you're getting the discount for
   * @param {Object} User The logged in user
   * @returns {Object} Discount object
   * @memberof API
   */
  getDiscount(itemId,
    { username } = { username: 'default' }) {
    return {
      ...this._defaultDiscount,
      ...this.DB.discounts.find(discount => discount.username === username
      && discount.appliesTo === itemId),
    }
  }

  // i return the item object that matches the itemId
  @cache(cacheOptions)
  getItem(itemId) {
    return this.DB.items.find(item => item.id === itemId)
  }

  getItemCount(items) {
    return price.getItemCount(items)
  }

  @cache(cacheOptions)
  getItems() {
    return this.DB.items
  }

  @cache(cacheOptions)
  getPrice(itemId) {
    return this.DB.items.find(item => item.id === itemId).price
  }

  @cache(cacheOptions)
  getCustomers() {
    return this.DB.customers
  }

  /**
   * Return rounded price to X decimals, and alo prevents rounding errors
   * Code was stolen from all over the internet
   * https://gist.github.com/ArminVieweg/28647e735aa6efaba401
   *
   * @param {*} value
   * @param {*} decimals
   * @returns {Number} rouded to two decimals
   * @memberof API
   */
  preciseRound(num, decimals = 2) {
    const sign = () => {
      // IE does not support method sign here
      if (typeof Math.sign === 'undefined') {
        if (num > 0) {
          return 1
        }
        if (num < 0) {
          return -1
        }
        return 0
      }
      return Math.sign(num)
    }

    const t = 10 ** decimals
    return (Math.round((num * t)
      + (decimals > 0 ? 1 : 0) * (sign(num) * (10 / (100 ** decimals)))) / t
    ).toFixed(decimals)
  }


  /**
   *
   *
   * @param {Array} items
   * @param {String} user
   * @returns {Number} total cost if items
   * @memberof Price
   */
  getTotalPrice(items, user) {
    let total = 0

    const itemsWithCount = price.getItemCount(items)
    // I have what's in the shopping cart, along with
    // the amount of units, and I iterate through
    Object.keys(itemsWithCount).forEach((itemId) => {
      // I get the price of the item
      const priceOfItem = this.getPrice(itemId)

      // I get the discount pertaining to that item
      const discount = this.getDiscount(itemId, user)

      // I get the total price of the SKU and sum it all up
      total += price.calcPrice(
        itemsWithCount[itemId],
        priceOfItem,
        discount,
      )

      // return total
    })

    return this.preciseRound(total)
  }
}

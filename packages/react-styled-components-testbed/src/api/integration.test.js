/*
 * NOTE: These are not unit tests. These are more like integration
 * tests to test API integrity & the database should be mocked properly
 *
 */

import API from './index'
import DB from './db.json' // should be mocked in the real world

const { getDiscount, getItem, getPrice } = new API(DB)

describe('getPrice()', () => {
  it('should return 269.99 if asked for the price of a classic ad', () => {
    expect(getPrice('classic')).toEqual(269.99)
  })

  it('should return 322.99 if asked for the price of a classic ad', () => {
    expect(getPrice('standout')).toEqual(322.99)
  })
})

describe('getItem()', () => {
  it('should return the classic item if we ask for a `classic` itemId', () => {
    const classicAd = {
      id: 'classic',
      name: 'Classic',
      description: 'Offers the most basic level of advertisement.',
      price: 269.99,
    }
    expect(getItem('classic')).toEqual(classicAd)
  })
})


describe('Config: getDiscount()', () => {
  const unilever = {
    username: 'unilever',
    password: 'u123',
    name: 'Unilever',
  }

  const ford = {
    username: 'ford',
  }

  it('should return unilever\'s classic discount when using itemId `classic` & user is unilever', () => {
    const unileversClassicDiscount = {
      appliesTo: 'classic', discount: 0, discountDescription: 'Get 3 for the price of 2 for Classic Ads!', itemsToGet: 3, itemsToPayFor: 2, minItemsToBuy: 0, username: 'unilever',
    }

    expect(getDiscount('classic', unilever)).toEqual(unileversClassicDiscount)
  })

  it('should return the correct discount for ford', () => {
    const fordsDiscount = getDiscount('classic', ford)
    expect(fordsDiscount.itemsToGet).toEqual(5)
    expect(fordsDiscount.itemsToPayFor).toEqual(4)
  })
  it('should return the correct discount for nike', () => {
    const nikesDiscount = getDiscount('premium', { username: 'nike' })
    expect(nikesDiscount.discountAmount).toEqual(379.99)
    expect(nikesDiscount.minItemsToBuy).toEqual(4)
  })
})

describe('calculates the total price', () => {
  const api = new API()

  it(`should return 987.97 when 'classic', 'standout',
     'premium' items added to the cart and user is default`, () => {
    const items = ['classic', 'standout', 'premium']
    expect(api.getTotalPrice(items, { username: 'default' })).toEqual('987.97')
  })

  it(`should return 934.97 when
     'classic', 'classic', 'classic', 'premium'
     items added to the cart and user is unilever`, () => {
    const items = ['classic', 'classic', 'classic', 'premium']
    expect(api.getTotalPrice(items, { username: 'unilever' })).toEqual('934.97')
  })

  const itemsForApple = ['standout', 'standout', 'standout', 'premium']
  it(`should return 1294.96 when items are ${itemsForApple.join(', ')} is bought by apple`, () => {
    expect(api.getTotalPrice(
      itemsForApple, { username: 'apple' }, api.getPrice, api.getDiscount
    )).toEqual('1294.96')
  })
})

describe('round to two decimals', () => {
  const api = new API()
  expect(api.preciseRound(19.9999)).toEqual('20.00')
  expect(api.preciseRound(1)).toEqual('1.00')
})

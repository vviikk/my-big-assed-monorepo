import styled from 'styled-components'
import CaretDown from 'react-icons/lib/fa/caret-down' // eslint-disable-line sort-imports
import CaretUp from 'react-icons/lib/fa/caret-up'
import CartIcon from 'react-icons/lib/md/shopping-cart'

// This class just returns most of the styled components for the project.
// Most CSS Stolen from Milligram https://milligram.io

export const Colors = `
  --color-initial: #fff;
  --color-primary: #9b4dca;
  --color-secondary: #606c76;
  --color-tertiary: #f4f5f6;
  --color-quaternary: #d1d1d1;
  --color-quinary: #e1e1e1;
`
// Should export styled-comonents like H1, H2
export const Typography = `
  b,
  strong {
    font-weight: bold;
  }

  p {
    margin-top: 0;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-weight: 300;
    letter-spacing: -0.1rem;
    margin-bottom: 2rem;
    margin-top: 0;
  }

  h1 {
    font-size: 4.6rem;
    line-height: 1.2;
  }

  h2 {
    font-size: 3.6rem;
    line-height: 1.25;
  }

  h3 {
    font-size: 2.8rem;
    line-height: 1.3;
  }

  h4 {
    font-size: 2.2rem;
    letter-spacing: -0.08rem;
    line-height: 1.35;
  }

  h5 {
    font-size: 1.8rem;
    letter-spacing: -0.05rem;
    line-height: 1.5;
  }

  h6 {
    font-size: 1.6rem;
    letter-spacing: 0;
    line-height: 1.4;
  }
`

export const GlobalCSS = `
  *,
  *:after,
  *:before {
    box-sizing: inherit;
  }

  html {
    box-sizing: border-box;
    font-size: 62.5%;
  }
  :root {
    ${Colors}
  }
  body {
    color: var(--color-secondary);
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    font-size: 1.6em;
    font-weight: 300;
    letter-spacing: 0.01em;
    line-height: 1.6;
    max-width: 60rem;
    margin: 0 auto;
    padding-top: 5rem;
  }
  ${Typography}
`

export const Button = styled.button`
  background-color: var(--color-primary);
  border: .1rem solid var(--color-primary);
  border-radius: .4rem;
  color: var(--color-initial);
  cursor: pointer;
  display: inline-block;
  font-size: 1.1rem;
  font-weight: 700;
  height: 3.8rem;
  letter-spacing: .1rem;
  line-height: 3.8rem;
  padding: 0 2rem;
  text-align: center;
  text-decoration: none;
  text-transform: uppercase;
  white-space: nowrap;

  &:focus,
  &:hover {
    background-color: var(--color-secondary);
    border-color: var(--color-secondary);
    color: var(--color-initial);
    outline: 0;
  }
  > svg {

  }
`

export const ButtonOutlined = Button.extend`
  background-color: transparent;
  color: var(--color-primary);

  &:focus,
  &:hover {
    background-color: transparent;
    border-color: var(--color-secondary);
    color: var(--color-secondary);
  }
  &[disabled]
    &:focus,
    &:hover {
      border-color: inherit;
      color: var(--color-primary);
    }
`

export const ButtonClear = ButtonOutlined.extend`
    border-color: transparent;
`

export const Select = styled.select`
  appearance: none;
  background-color: transparent;
  border: .1rem solid var(--color-quaternary);
  border-radius: .4rem;
  box-shadow: none;
  box-sizing: inherit;
  height: 3.8rem;
  padding: .6rem 1.0rem;
  width: 100%;
  background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="14" viewBox="0 0 29 14" width="29"><path fill="%23d1d1d1" d="M9.37727 3.625l5.08154 6.93523L19.54036 3.625"/></svg>') center right no-repeat;
  padding-right: 3.0rem;
  margin-bottom: 1.9rem;
  color: var(--color-primary);

  &:focus {
    border-color: var(--color-primary);
    outline: 0;
    background-image: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="14" viewBox="0 0 29 14" width="29"><path fill="%239b4dca" d="M9.37727 3.625l5.08154 6.93523L19.54036 3.625"/></svg>');
  }
`
export const Label = styled.label`
  display: block;
  font-size: 1.6rem;
  font-weight: 700;
  margin-bottom: .5rem;

`

export const Icons = {
  Cart: CartIcon,
  CaretDown,
  CaretUp,
}

export const Badge = styled.span`
  background-color: var(--color-primary);
  display: inline-block;
  color: white;
  margin-right: 1ch;
  font-size: .9em;
  font-weight: bold;
  text-transform: uppercase;
  padding: 0 .5ch;
  border-radius: 2px;
  line-height: 1;
`

export const Navbar = styled.div`
  box-shadow: 9999px 0 0 black;
  border-bottom: 1px solid var(--color-tertiary);
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  min-height: 5rem;
  padding-top: .25rem;
  padding-bottom: .25rem;
  max-width: 60rem;
  margin: 0 auto;
  > h2 {
    margin: 0;
    padding: 0;
  }
`
export const DropdownWrapper = styled.div`
  position: relative;
  display: flex;
`
export const DropdownDiv = styled.div`
  position: absolute;
  top: 4rem;
  right: 0;
  left: 0;
  background: white;
  border: 1px solid var(--color-primary);
  border-radius: 3px;
  padding: .5rem;
  display: flex;
  flex-direction: column;
  &:before {
    content: '';
    position: absolute;
    top: -11px;
    right: 1  0px;
    border-bottom: 11px solid var(--color-primary);
    border-left: 11px solid transparent;
    border-right: 11px solid transparent;
  }

  &:after {
    content: '';
    position: absolute;
    right: 11px;
    top: -10px;
    border-bottom: 10px solid #fff;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
  }
`


export const H4 = styled.h4`
  margin: 0;
  text-transform: uppercase;
`


export const H6 = styled.h6`
  color: var(--color-primary);
  float: left;
`

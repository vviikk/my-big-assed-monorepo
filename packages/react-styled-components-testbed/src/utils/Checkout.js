import API from '../api'

const api = new API()

export default class Checkout {
  constructor(user = { username: 'default' }) {
    this.user = user
    this.items = []
    this.addItem = this.addItem.bind(this)
    this.getItems = this.getItems.bind(this)
    this.total = this.total.bind(this)
  }

  addItem(item) {
    this.items.push(item)
  }

  getItems() {
    return this.items
  }

  getItemCount() {
    return api.getItemCount(this.items)
  }

  total() {
    return api.getTotalPrice(this.items,
      this.user)
  }
}

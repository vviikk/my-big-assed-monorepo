/* eslint-disable sort-imports */
import React, { Component } from 'react'

import styled, { injectGlobal } from 'styled-components'
import PropTypes from 'prop-types'

import {
  Button, GlobalCSS, H6, Navbar,
} from './utils/Style'

import API from './api' // eslint-disable-line sort-imports
import Cart from './components/Cart' // eslint-disable-line sort-imports
import Catalog from './components/Catalog'
import Checkout from './utils/Checkout'
import Login from './components/Login'

const CartSidebar = styled.aside`
  float: right;
`

injectGlobal`
  ${GlobalCSS}
`

class App extends Component {
  constructor(props) {
    super(props)
    this.API = new API()
    this.state = { items: [], isLoggedIn: false }
    this.handleClick = this.handleClick.bind(this)
    this.handleCleanCart = this.handleCleanCart.bind(this)
    this.state.checkout = new Checkout(this.props.user)
  }

  handleClick(item) {
    // This part would not be necessary if I used a state management system
    // React should do mutation
    this.state.checkout.addItem(item.id)
    this.setState({
      items: this.state.checkout.items,
    })
  }

  updateCustomer = user => () => {
    this.setState({
      user,
      items: [],
      checkout: new Checkout(user),
      isLoggedIn: true,
    })
  }

  handleCleanCart = () => {
    this.setState({
      checkout: new Checkout(this.state.user),
      items: [],
    })
  }

  doLogout = () => {
    this.setState({
      user: {},
      checkout: new Checkout(),
      items: [],
      isLoggedIn: false,
    })
  }

  render() {
    if (!this.state.isLoggedIn) {
      return <Login users={this.API.getCustomers()} onLogin={this.updateCustomer}/>
    }
    return (
      <div className="App">
        <Navbar>
          <H6>Hi, {this.state.user.username}!</H6>
          <CartSidebar>
            <Button onClick={this.doLogout}>Logout</Button>
            <Cart
              count={this.state.items.length}
              checkout={this.state.checkout}
              onCleanCart={this.handleCleanCart}
            />
          </CartSidebar>
        </Navbar>
        <Catalog
          handleCardClick={this.handleClick}
          user={this.state.user}
        />
      </div>
    )
  }
}

export default App

App.propTypes = {
  user: PropTypes.object,
}

App.defaultProps = {
  user: {
    name: 'Default User',
    username: 'default',
  },
}

import styled from 'styled-components'

export const ItemCard = styled.div`
  padding: .5rem;
  border-radius: 3px;
  margin: 1rem;
  background: var(--color-tertiary);
  border: 1px solid var(--color-quaternary);
  cursor: pointer;
  display: flex;

  :hover {
    border-color: var(--color-primary);
  }
  :active {
    background-color: var(--color-primary);
    color: var(--color-initial);
  }
`

export const InfoDiv = styled.div`
  flex: 1;
`

export const PriceDiv = styled.h4``.extend`
  display: flex;
  flex-direction: row;
  align-items: center;
  text-align: right;
  width: 10rem;
  font-weight: bolder;
  padding-left: 1rem;
  color: var(--primary-color);
`

export const ItemsWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

/* eslint-disable sort-imports */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Badge, H4 } from '../../utils/Style'
import API from '../../api'
import {
  InfoDiv, ItemCard, ItemsWrapper, PriceDiv,
} from './StyledComponents'

const api = new API()

export default class Catalog extends Component {
  render() {
    const items = api.getItems()
    const renderDiscount = (itemId) => {
      const discount = api.getDiscount(itemId, this.props.user)

      return discount.discountDescription
        ? <p><Badge>Discount</Badge>{discount.discountDescription}</p>
        : ''
    }

    const renderPrice = (itemId, priceWithoutDiscount) => {
      const discount = api.getDiscount(itemId, this.props.user)
      return (discount.isFixedDiscount && discount.minItemsToBuy < 1)
        ? discount.discountAmount
        : priceWithoutDiscount
    }
    return (
      <Fragment>
        <ItemsWrapper>
          {items.map(item => <ItemCard
            key={item.id}
            onClick={() => this.props.handleCardClick(item)}

            >
            <InfoDiv>
              <H4>{item.name}</H4>
              <p>{item.description}</p>
              {renderDiscount(item.id)}
            </InfoDiv>

            <PriceDiv>
              ${renderPrice(item.id, item.price)}
            </PriceDiv>
          </ItemCard>)}
        </ItemsWrapper>
      </Fragment>
    )
  }
}

Catalog.propTypes = {
  handleCardClick: PropTypes.func,
  user: PropTypes.object,
}

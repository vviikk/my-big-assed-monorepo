const API_URL = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=b5ce47ee7d5f3866faa9bb868117a952&nojsoncallback=1&sort=relevance&format=json&extras=url_s,description&per_page=5&text='
const API_URL_PUBLIC = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&extras=url_s,description}&tags='

// template for loading spinner
const loadingSpinner = '<div class=\'loading-spinner\'/>'

// select multiple elements
const $ = document.querySelectorAll.bind(document)
// select specific single items
const $$ = document.querySelector.bind(document)

// Cache some reusable reselctors
const $$output = $$('#output')
const $$outputPayload = $$('#payload')

const useJsonP = _ => $$('#use-jsonp').checked // invoke this later after we're sure document is ready
const useOutputCode = _ => $$('#use-code-output').checked // invoke this later after we're sure document is ready

// Ignore this function
// I only wanted to debug JSONP payloads easily as it's more
// of a pain than rest. Left it in cuz it looks cool :)
// https://stackoverflow.com/a/7220510/430773
const syntaxHighlight = (json) => {
  const outputJson = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
  return outputJson.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\\-]?\d+)?)/g, (match) => {
    let cls = 'number'
    if (/^"/.test(match)) {
      if (/:$/.test(match)) {
        cls = 'key'
      } else {
        cls = 'string'
      }
    } else if (/true|false/.test(match)) {
      cls = 'boolean'
    } else if (/null/.test(match)) {
      cls = 'null'
    }
    return `<span class="${cls}">${match}</span>`
  })
}

// ES2015 version of lodash's _.get() command
const get = (value, path, defaultValue = null) => path.split('.').reduce((acc, v) => {
  try {
    acc = acc[v]
  } catch (e) {
    return defaultValue
  }
  return acc
}, value)

// Extend document object with async helper functions
// for document load status - uses promises :)
const $$doc = Object.assign(document, {
  on: (fn, event = 'DOMContentLoaded') => $$doc.addEventListener(event, fn),
  ready: () => new Promise((resolve) => {
    if ($$doc.readyState === 'complete') {
      resolve()
    } else {
      $$doc.on(resolve)
    }
  }),
})

// Output function to write the images into the dom
const output = (html) => { $$output.innerHTML = html }

// writes the loading spinner into the output
const showLoadingSpinner = _ => output(loadingSpinner)

// Function that is called when jsonp callback is fired
const loadImages = (data, photoObjectLocation = 'items', photoSrc = 'media.m') => {
  const outputString = `<ul class='grid'>${
    get(data, photoObjectLocation, [])
      .slice(0, 5) // not needed for REST, but needs to be done here for JSONP
    // https://stackoverflow.com/questions/47003390/with-flickr-api-is-it-possible-to-list-only-10-item-in-the-feed
      .map(photo => `<li><img src='${get(photo, photoSrc)}'/></li>`).join('') // eslint-disable-line camelcase
  }INSERT_PRE_HERE</ul>`

  const code = (useOutputCode() && data)
    ? syntaxHighlight(JSON.stringify(data, null, 2))
    : ''

  const wrapCode = _code => `<pre class="ft-syntax-highlight scrollable" id="payload" data-syntax="js" data-syntax-theme="one-dark">${_code}</pre>`

  output(outputString.split('INSERT_PRE_HERE').join(wrapCode(code)))
}

window.loadImages = loadImages // global namespace needed for jsonp

// Uses Flickr's public API. The public feed API only supports CORS
const fetchJsonP = (url, parameter = 'jsoncallback', callbackFunction = 'loadImages') => {
  const script = document.createElement('script')
  const cleanup = r => (x) => {
    document.head.removeChild(script)
    r(x)
  }

  return new Promise((resolve, reject) => {
    window[callbackFunction] = (...args) => {
      window[callbackFunction]()
      cleanup(resolve)
    }
    // script.onload = cleanup(resolve)
    script.onerror = cleanup(reject)
    // setTimeout(script.onerror, 5000); might be advisable

    script.src = `${url}&${parameter}=${callbackFunction}`
    document.head.appendChild(script)
  })
}

// Simple async wrapper for REST requests
const fetchJsonAsync = url => new Promise((resolve, reject) => {
  const req = Object.assign(
    new XMLHttpRequest(),
    {
      onload: () => {
        if (req.status === 200) {
          resolve(JSON.parse(req.response))
        } else {
          reject(new Error(req.statusText))
        }
      },
      onerror: () => {
        reject(new Error('Network error'))
      },
    }
  )
  req.open('GET', url)
  req.send()
})


const getApiUrl = (shouldUseJsonP = useJsonP()) => shouldUseJsonP
  ? API_URL_PUBLIC
  : API_URL

const getFullUrl = animal => getApiUrl() + animal

const getFetchFunction = (
  target,
  shouldUseJsonP = useJsonP()
) => (
  shouldUseJsonP ? fetchJsonP : fetchJsonAsync
)(getFullUrl(target))

let lastAnimal = null // @TODO avoid using let/null

const onclick = ({ target = false }, useLast) => {
  showLoadingSpinner()
  lastAnimal = useLast ? lastAnimal : target.attributes['data-animal'].value
  getFetchFunction(lastAnimal).then(r => loadImages(r, 'photos.photo', 'url_s'))
}

const boot = async () => {
  await $$doc.ready()

  $('button[data-animal]').forEach(el => Object.assign(el, { onclick }))
  // The following code just reuses the onclick function but passing in a null argument,
  // essentially getting the payload again
  $('input.switch').forEach(el => Object.assign(
    el,
    { onclick: onclick.bind(null, false, true) } // use .bind() for partials :)
  ))

  $('button[data-animal]')[0].click()

  // Just pulling all logic into deciding which animal to fetch
  // away from this toggle.
}

boot()

const request = require('request')
const md = require('markdown-it')({
  html: true,
  linkify: true,
  typographer: true,
  breaks: true,
  highlight(str, lang) {
    return `<pre class="hljs">${md.utils.escapeHtml(str).split('\n').join('<br>')}</pre>`
  },
})

const Gist = require('gist.js')

module.exports = async (req, res) => {
  const writeMdAsHtml = (mdToConvert) => {
    const body = `<html><body>${md.render(mdToConvert)}</body></html>`
    res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' })
    res.write(body, 'utf-8')
    res.end()
  }

  const writeError = (_ = '') => { res.writeHead(504); res.end(`Error${_}`) }

  const getRawFile = (url) => {
    request.get(url, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        writeMdAsHtml(body)
      }
    })
  }

  const getGist = (gistId) => {
    console.log(`Getting gist: ${gistId}`)
    const parseGist = (...g) => {
      const gistToConvert = Object.entries(g[1].files)[0][1].content
      writeMdAsHtml(gistToConvert)
    }
    try {
      Gist(gistId).get(parseGist)
    } catch (e) {
      console.log(e)
      res.end(`Error${e}`)
    }
  }

  const locationOfMarkdown = req.url.replace('/', '')
  const isGist = locationOfMarkdown.match(/\b[0-9a-f]{5,40}\b/g) || []
  if (locationOfMarkdown.indexOf('http') !== -1) {
    getRawFile(locationOfMarkdown)
  } else if (isGist.length) {
    getGist(locationOfMarkdown)
  } else { writeError() }

  setTimeout(writeError.bind(null, '...timeout after 10 seconds'), 10000)
}

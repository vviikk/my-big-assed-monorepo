// server.js

const express = require('express')

const port = process.env.PORT || 8080
const app = express()

app.get('/api', (req, res) => {
  res.send(JSON.stringify({ name: 'Kaylie' }))
})

const staticSites = [
  {
    name: 'css-only-card-flip',
  },
  {
    name: 'flickr-vanilla-javascript',
  },
]

const getStaticPath = site => `./packages/${site.name}/build`
// Make the directory available for serving assets statically
staticSites.forEach((site) => {
  app.use(`/${site.name}`, express.static(getStaticPath(site)))
  console.log(`${site.name}is being served at ${getStaticPath(site)}`)
})

app.use('/', express.static('./public'))

// Define a route for your app
app.listen(port, () => console.log(`BB is listening on port ${port}!`))
